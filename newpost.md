---
layout: default
title: New Post
---
Here is some content for the post that is below the "formatter"
1.	Download Patch
2.	Copy downloaded patch to location on server 
a.	For example: /tmp
3.	Unzip into directory
a.	For example: unzip -d patching-files p26755797_122130_Generic.zip
4.	Locate the OPatch Utility
a.	ORACLE_HOME/Opatch/
b.	It is in same directory as wlserver
5.	Add the OPatch location to the PATH
a.	export PATH=$PATH:/apps/path/to/oracle_home/Opatch
b.	example: export PATH=$PATH:/u02/oracle/products/fmw/OPatch/
6.	From the location of patch
a.	opatch -version
b.	list the installed patches
i.	opatch lsinentory
c.	install the patch
i.	opatch apply
1.	Error: Exception occured :     fuser could not be located:
2.	Resolution: export OPATCH_NO_FUSER=true , then re-run opatch apply.  For more info see Doc ID 2429708.1.
